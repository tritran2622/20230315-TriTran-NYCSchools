//
//  SchoolsBookmarkViewController.swift
//  20230315-TriTran-NYCSchools
//
//  Created by Tri Tran on 3/15/23.
//

import UIKit

class SchoolsBookmakViewController: UIViewController {

    // Storyboard links properties
    @IBOutlet weak var schoolSearchBar: UISearchBar!
    @IBOutlet weak var schoolsTableView: UITableView!
    
    // Properties declaration
    var schoolsData: [Schools] = []
    var filterSchools: [Schools] = []
    var selectedSchool: School?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        schoolsTableView.dataSource = self
        schoolsTableView.delegate = self
        schoolsTableView.estimatedRowHeight = 80
        
        schoolSearchBar.delegate = self
    }
    
    // Refresh data before scence appear on screen
    override func viewWillAppear(_ animated: Bool) {
        schoolsData = NYCSDBManager.shared.getBookmarkSchools()
        filterSchools = schoolsData
        schoolsTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewSchoolSatScore2" {
            let destinationVC = segue.destination as! SchoolSatScorelViewController
            destinationVC.school = selectedSchool
        }
    }
}

// MARK: - UITableView Delegate
extension SchoolsBookmakViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterSchools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "schoolBookmakrTableViewCell") as? SchoolBookmarkTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(school: filterSchools[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Prepare school object to send to SchoolSatScoreViewController
        // when user tab a cell
        guard let selectedSchoolName = filterSchools[indexPath.row].schoolName,
                let selectedSchoolId = filterSchools[indexPath.row].schoolId else {return}
        selectedSchool = School(schoolName: selectedSchoolName, schoolId:selectedSchoolId)
        self.performSegue(withIdentifier: "viewSchoolSatScore2", sender: self)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            // Delete operations when swipe left
            schoolsTableView.beginUpdates()
            NYCSDBManager.shared.deleteSchool(school: filterSchools[indexPath.row])
            schoolsData.removeAll(where: {$0 == filterSchools[indexPath.row]})
            filterSchools.remove(at: indexPath.row)
            schoolsTableView.deleteRows(at: [indexPath], with: .automatic)
            schoolsTableView.endUpdates()
        }
    }
    
    // Dismiss keyboard when scroll
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        schoolSearchBar.endEditing(true)
    }
}

// MARK: - UISearchBar Delegate
extension SchoolsBookmakViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterSchools = []
        
        // Load full data if searchbar text is empty
        if searchText.isEmpty {
            filterSchools = schoolsData
        }
        
        // Get list of all school names that contains
        // searchbar text
        for school in schoolsData {
            if let check = school.schoolName?.uppercased().contains(searchText.uppercased()), check {
                filterSchools.append(school)
            }
        }
        
        self.schoolsTableView.reloadData()
    }
    
    // Dismiss keyboard after press search
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        schoolSearchBar.endEditing(true)
    }
}
