//
//  SchoolTableViewCell.swift
//  20230315-TriTran-NYCSchools
//
//  Created by Tri Tran on 3/15/23.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {

    // Storyboard links properties
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var schoolName: UILabel!
    
    // Initial properties
    private var isBookmark = false
    private var school: School!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // Helper function to set up the cell
    // with school name and is bookmarked
    func configure(school: School) {
        schoolName.text = school.schoolName
        self.school = school
        isBookmark = NYCSDBManager.shared.checkSchoolExistInDB(schoolId: school.schoolId)
        changeStarColor()
    }
    
    // Choose the approriate actions when
    // user tap the bookmark icon
    @IBAction func handleMarkAsFavorite() {
        isBookmark ?
        NYCSDBManager.shared.deleteSchool(schoolId: school.schoolId) :
            NYCSDBManager.shared.saveSchool(schoolDM: school)
        isBookmark = !isBookmark
        changeStarColor()
    }
    
    // Helping func to change color of the bookmark icon
    func changeStarColor() {
        starButton.tintColor = isBookmark ? .red : .gray
    }
}
