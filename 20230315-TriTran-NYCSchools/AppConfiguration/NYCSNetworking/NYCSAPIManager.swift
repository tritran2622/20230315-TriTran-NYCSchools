//
//  NYCSAPIManager.swift
//  20230315-TriTran-NYCSchools
//
//  Created by Tri Tran on 3/15/23.
//

import Foundation

class NYCSAPIManager {
    static let shared = NYCSAPIManager()
    private init(){}
    
    // Function to get school names from API link
    func getSchoolNames(completionHandler: @escaping([School]?, Error?) -> Void) {
        
        // Prepare URL for HTTP request
        guard let url = URL(string: ConstantVars.baseUrl + ConstantVars.schoolsFile + "?" + ConstantVars.sqlSelect + ConstantVars.shoolsFilter + "," + ConstantVars.schoolIdFilter) else {return}
        
        // Set properties for HTTP request
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        // Connect to API using HTTP request and get data
        URLSession.shared.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            
            // Pass error to completionHandler
            if let error = error {
                completionHandler(nil, error)
            }
            
            // Pair the data from API to School Data Model
            // and pass it to completion handler
            guard let data1 = data else {return}
            do {
                let apiResult = try JSONDecoder().decode([School].self, from: data1)
                completionHandler(apiResult, nil)
            }
            // If error happen during the data conversion
            // pass the error to completion handler
            catch {
                print(error)
                completionHandler(nil, error)
            }
        }).resume()
    }
    
    // Function to get school sat score from API link
    func getSchoolSatScore(schoolId: String, completionHandler: @escaping([SchoolSatScore]?, Error?) -> Void) {
        
        // Prepare URL for HTTP request
        guard let url = URL(string: ConstantVars.baseUrl + ConstantVars.schoolSatFile + "?" + ConstantVars.schoolIdFilter + "=" + schoolId) else {return}
        
        // Set properties for HTTP request
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        // Connect to API using HTTP request and get data
        URLSession.shared.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            
            // Pass error to completionHandler
            if let error = error {
                completionHandler(nil, error)
            }
            
            // Pair the data from API to School Data Model
            // and pass it to completion handler
            guard let data1 = data else {return}
            do {
                let apiResult = try JSONDecoder().decode([SchoolSatScore].self, from: data1)
                completionHandler(apiResult, nil)
            }
            
            // If error happen during the data conversion
            // pass the error to completion handler
            catch {
                print(error)
                completionHandler(nil, error)
            }
        }).resume()
    }
}
