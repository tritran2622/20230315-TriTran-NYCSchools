//
//  NYCSchoolsDataModel.swift
//  20230315-TriTran-NYCSchools
//
//  Created by Tri Tran on 3/15/23.
//

import Foundation

// School Data Model for API and
// object use through the app
struct School: Codable {
    let schoolName: String
    let schoolId: String
    
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case schoolId = "dbn"
    }
}
