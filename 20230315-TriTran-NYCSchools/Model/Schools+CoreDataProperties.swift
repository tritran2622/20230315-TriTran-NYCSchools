//
//  Schools+CoreDataProperties.swift
//  20230315-TriTran-NYCSchools
//
//  Created by Tri Tran on 3/15/23.
//
//

import Foundation
import CoreData


extension Schools {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Schools> {
        return NSFetchRequest<Schools>(entityName: "Schools")
    }

    @NSManaged public var schoolId: String?
    @NSManaged public var schoolName: String?

}

extension Schools : Identifiable {

}
