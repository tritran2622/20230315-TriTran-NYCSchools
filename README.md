#  NYCSchools
NYCSchools is a mini demo app that shows a list of schools in New York City and lets you check and bookmark schools for theirs SAT scores

Technology Usage in project:
Utilized CoreData framework to persistently store schools bookmark so user can check them later on.
Created a layout that fits the screen of iOS device and asynchronously load data from API using URLSession and display them on UITableView.
Applying MVVM, Singleton, Delegate concept for easier to read, control, and track the flow of the app.

How to use:
https://streamable.com/rh7yjv
